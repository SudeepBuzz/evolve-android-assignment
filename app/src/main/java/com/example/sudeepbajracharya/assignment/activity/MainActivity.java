package com.example.sudeepbajracharya.assignment.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.sudeepbajracharya.assignment.adapter.PostAdapter;
import com.example.sudeepbajracharya.assignment.api.ApiClient;
import com.example.sudeepbajracharya.assignment.model.Post;
import com.example.sudeepbajracharya.assignment.interfaces.ApiInterface;
import com.example.sudeepbajracharya.assignment.interfaces.OnDataItemClickListener;
import com.example.sudeepbajracharya.assignment.R;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private PostAdapter adapter;
    private List<Post> posts;
    private ApiInterface apiInterface;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(config);

        boolean connected = checkNetworkAvailability(getApplicationContext());
        if (connected == true) {
            apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

            Call<List<Post>> call = apiInterface.getData();
            call.enqueue(new Callback<List<Post>>() {
                @Override
                public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                    posts = response.body();
                    adapter = new PostAdapter(posts);
                    recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
                    recyclerView.setAdapter(adapter);
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.insertOrUpdate(posts);
                        }
                    });
                    adapter.setOnDataItemClickListener(new OnDataItemClickListener() {
                        @Override
                        public void onDataClick(Post data, int position) {
                            Intent i = new Intent(MainActivity.this, CommentActivity.class);
                            i.putExtra("identity", position + 1);
                            startActivity(i);
                        }
                    });
                }
                @Override
                public void onFailure(Call<List<Post>> call, Throwable t) {

                }
            });
        }
        else
            retrive();
    }

    private void retrive() {
        Toast.makeText(getApplication(), "No internet connection trying to fetch data stored", Toast.LENGTH_SHORT).show();
        RealmResults<Post> result = realm.where(Post.class).findAllAsync().sort("id");
        result.load();
        adapter = new PostAdapter(result);
        recyclerView.setAdapter(adapter);

        adapter.setOnDataItemClickListener(new OnDataItemClickListener() {
            @Override
            public void onDataClick(Post data, int position) {
                Intent i = new Intent(MainActivity.this, CommentActivity.class);
                i.putExtra("identity", position + 1);
                startActivity(i);
            }
        });
    }

    public static boolean checkNetworkAvailability(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return (networkInfo != null) && (networkInfo.isConnected());

    }
}
