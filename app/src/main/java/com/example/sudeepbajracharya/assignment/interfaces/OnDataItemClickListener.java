package com.example.sudeepbajracharya.assignment.interfaces;

import com.example.sudeepbajracharya.assignment.model.Post;


public interface OnDataItemClickListener {
    void onDataClick(Post data, int position);
}
