package com.example.sudeepbajracharya.assignment.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sudeepbajracharya.assignment.model.Comment;
import com.example.sudeepbajracharya.assignment.R;

import java.util.List;


public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.DetailViewHolder>{
    private List<Comment> comments;
    public CommentAdapter(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public DetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item,parent,false);
        return new DetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DetailViewHolder holder, int position) {
        holder.TvPostId.setText(String.valueOf(comments.get(position).getPostId()));
        holder.TvId.setText(String.valueOf(comments.get(position).getId()));
        holder.TvName.setText(comments.get(position).getName());
        holder.TvEmail.setText(comments.get(position).getEmail());
        holder.TvBody.setText(comments.get(position).getBody());
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }
    public class DetailViewHolder extends RecyclerView.ViewHolder{

        TextView TvPostId, TvId, TvName, TvEmail, TvBody;
        public DetailViewHolder(View itemView) {
            super(itemView);

            TvPostId = (TextView) itemView.findViewById(R.id.tvPostId);
            TvId = (TextView) itemView.findViewById(R.id.tvId);
            TvName = (TextView) itemView.findViewById(R.id.tvName);
            TvEmail = (TextView) itemView.findViewById(R.id.tvEmail);
            TvBody = (TextView) itemView.findViewById(R.id.tvBody);
        }
    }
}