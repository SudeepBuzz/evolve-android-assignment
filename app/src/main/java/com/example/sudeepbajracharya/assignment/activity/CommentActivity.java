package com.example.sudeepbajracharya.assignment.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.sudeepbajracharya.assignment.adapter.CommentAdapter;
import com.example.sudeepbajracharya.assignment.api.ApiClient;
import com.example.sudeepbajracharya.assignment.model.Comment;
import com.example.sudeepbajracharya.assignment.interfaces.ApiInterface;
import com.example.sudeepbajracharya.assignment.R;

import java.util.List;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CommentAdapter adapter;
    private List<Comment> comments;
    private ApiInterface apiInterface;
    private Intent intent;
    private Realm realmComment;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        realmComment = Realm.getInstance(config);
        intent = getIntent();
        id = intent.getIntExtra("identity",0);
        recyclerView = (RecyclerView) findViewById(R.id.DetailRecyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        boolean connected = checkNetworkAvailability(getApplicationContext());
        if(connected == true) {
            apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            Call<List<Comment>> call = apiInterface.getComment(id);
            call.enqueue(new Callback<List<Comment>>() {
                @Override
                public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                    comments = response.body();
                    adapter = new CommentAdapter(comments);
                    recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
                    recyclerView.setAdapter(adapter);
                    realmComment.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.insertOrUpdate(comments);
                        }
                    });
                }
                @Override
                public void onFailure(Call<List<Comment>> call, Throwable t) {

                }
            });
        }
        else
            retrive();
    }

    private void retrive() {
        RealmResults<Comment> result = realmComment.where(Comment.class).equalTo("postId",id).findAll().sort("id");
        result.load();
        adapter = new CommentAdapter(result);
        recyclerView.setAdapter(adapter);
    }

    public static boolean checkNetworkAvailability(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return (networkInfo != null) && (networkInfo.isConnected());

    }
}