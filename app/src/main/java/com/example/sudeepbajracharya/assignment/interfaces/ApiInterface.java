package com.example.sudeepbajracharya.assignment.interfaces;

import com.example.sudeepbajracharya.assignment.model.Post;
import com.example.sudeepbajracharya.assignment.model.Comment;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiInterface {
    @GET("posts")
    Call<List<Post>> getData();

    @GET("posts/{id}/comments")
    Call<List<Comment>> getComment(@Path("id") int id);
}
