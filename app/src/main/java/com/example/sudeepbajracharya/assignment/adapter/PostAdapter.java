package com.example.sudeepbajracharya.assignment.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sudeepbajracharya.assignment.interfaces.OnDataItemClickListener;
import com.example.sudeepbajracharya.assignment.R;
import com.example.sudeepbajracharya.assignment.model.Post;

import java.util.List;


public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {
    private List<Post> posts;
    private OnDataItemClickListener onDataItemClickListener;

    Post data;
    public PostAdapter(List<Post> posts){
        this.posts = posts;

    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_items,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder( MyViewHolder holder, final int position) {
        data = posts.get(position);
        holder.TvUserId.setText(String.valueOf(posts.get(position).getUserId()));
        holder.TvId.setText(String.valueOf((posts.get(position).getId())));
        holder.TvTitle.setText(posts.get(position).getTitle());
        holder.TvBody.setText(posts.get(position).getBody());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDataItemClickListener.onDataClick(data, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }
    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView TvUserId, TvId, TvTitle, TvBody;
        public MyViewHolder(View itemView) {
            super(itemView);
            TvUserId = (TextView) itemView.findViewById(R.id.txtUserId);
            TvId = (TextView) itemView.findViewById(R.id.txtId);
            TvTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            TvBody = (TextView) itemView.findViewById(R.id.txtBody);
        }
    }
    public void setOnDataItemClickListener(OnDataItemClickListener onDataItemClickListener){
        this.onDataItemClickListener = onDataItemClickListener;

    }
}
